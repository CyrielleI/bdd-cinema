
/* Création de la BDD */

CREATE DATABASE IF NOT EXISTS Cinema_Patay CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci

/* Création de la table: Cinema */

CREATE TABLE Cinema(
        id_cinema   Int  AUTO_INCREMENT  NOT NULL PRIMARY KEY,
        nom         Varchar (50) NOT NULL,
        adresse     Varchar (250) NOT NULL,
        code_postal Varchar (10) NOT NULL,
        ville       Varchar (250) NOT NULL,
        description Text NOT NULL
)ENGINE=InnoDB;


/* Création de la table: Salle */

CREATE TABLE Salle(
        id_salle             Int  AUTO_INCREMENT  NOT NULL PRIMARY KEY,
        nom                  Varchar (250) NOT NULL,
        nom_de_place         Integer NOT NULL,
        id_cinema            Int,   
        FOREIGN KEY (id_cinema) REFERENCES Cinema(id_cinema)
)ENGINE=InnoDB;

/* Modification du nom de la ligne 'nom_de_place' par 'nombre_de_place'suite à erreur de saisie */

ALTER TABLE Salle CHANGE nom_de_place nombre_de_place INT;


/* Création de la table: Film */

CREATE TABLE Film(
        id_film        Int  AUTO_INCREMENT  NOT NULL PRIMARY KEY,
        titre          Varchar (250) NOT NULL,
        synopsis       Text NOT NULL,
        duree          Time NOT NULL,
        date_de_sortie Date NOT NULL,
        realise_par    Varchar (250) NOT NULL
)ENGINE=InnoDB;


/* Création de la table: Séance */

CREATE TABLE Seance(
        id_seance            Int  AUTO_INCREMENT  NOT NULL PRIMARY KEY,
        date_et_heure_seance Datetime NOT NULL,
        id_salle             Int,
        id_Film              Int,  
        FOREIGN KEY (id_salle) REFERENCES Salle(id_salle),
	FOREIGN KEY (id_film) REFERENCES Film(id_film)
)ENGINE=InnoDB;


/* Création de la table: utilisateur */

CREATE TABLE Utilisateur(
        id_utilisateur Int  AUTO_INCREMENT  NOT NULL PRIMARY KEY,
        nom            Varchar (50) NOT NULL,
        prenom         Varchar (50) NOT NULL,
        e_mail         Varchar (150) NOT NULL,
        password       Varchar (100) NOT NULL,
        admin          Boolean NOT NULL
)ENGINE=InnoDB;


/* Création de la table: Réservation */

CREATE TABLE Reservation(
        id_reservation Int  AUTO_INCREMENT  NOT NULL PRIMARY KEY,
        created        Datetime NOT NULL,
        id_seance      Int,
        id_utilisateur Int,
	    FOREIGN KEY (id_seance) REFERENCES Seance(id_seance),
	    FOREIGN KEY (id_utilisateur) REFERENCES Utilisateur(id_utilisateur)
)ENGINE=InnoDB;


/* Création de la table: Paiement */

CREATE TABLE Paiement(
        id_paiement      Int  AUTO_INCREMENT  NOT NULL PRIMARY KEY,
        type_de_paiement Varchar (50) NOT NULL,
        created          Datetime NOT NULL,
        id_reservation   Int,
        FOREIGN KEY (id_reservation) REFERENCES Reservation(id_reservation)
)ENGINE=InnoDB;


/* Création de la table: Tarif */

CREATE TABLE Tarif(
        id_tarif    Int  AUTO_INCREMENT  NOT NULL PRIMARY KEY,
        nom         Varchar (50) NOT NULL,
        prix        DECIMAL (15,3)  NOT NULL,
        description Text NOT NULL
)ENGINE=InnoDB;


/* Création de la table: Tarifs_Seances */

CREATE TABLE Tarifs_Seances(
        id_seance      Int,
        id_tarif      Int,
	    FOREIGN KEY (id_seance) REFERENCES Seance(id_seance),
	    FOREIGN KEY (id_tarif) REFERENCES Tarif(id_tarif)
)ENGINE=InnoDB;

/* Insertion de données dans la table 'Cinéma' */

INSERT INTO `Cinema` (`nom`,`adresse`,`code_postal`,`ville`,`description`)
VALUES
  ("ultrices","Ap #795-8458 Nec Rd.","46836","Huesca","vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend, nunc risus varius"),
  ("Maecenas","5922 Integer Av.","13784","Erli","convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc"),
  ("ac","5997 Adipiscing Street","99238","Pervomaisk","malesuada fames ac turpis egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed sem egestas blandit. Nam nulla magna, malesuada vel, convallis in, cursus et, eros. Proin ultrices. Duis volutpat"),
  ("lobortis","Ap #468-8326 Eget Rd.","22013","Emmen","varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem. Donec elementum, lorem ut aliquam iaculis, lacus pede sagittis augue, eu tempor erat neque non quam."),
  ("ut","Ap #723-7798 Ante St.","20314","San Jose del Monte","sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend, nunc risus varius orci, in consequat enim diam vel arcu. Curabitur ut odio vel est tempor bibendum.");

/* Insertion de données dans la table 'salle' */

INSERT INTO `Salle` (`nom`,`nombre_de_place`,`id_cinema`)
VALUES
  ("quis",218,5),
  ("ante",295,3),
  ("molestie",86,5),
  ("Donec",386,2),
  ("non,",166,4),
  ("lobortis",79,4),
  ("consequat",234,5),
  ("nec",142,4),
  ("sociosqu",134,2),
  ("hendrerit",340,1),
  ("sagittis.",165,4),
  ("mauris",306,3),
  ("urna",288,3),
  ("malesuada.",328,2),
  ("arcu",113,1),
  ("cursus",186,2),
  ("ipsum",318,4),
  ("ultricies",65,1),
  ("ac",344,2),
  ("ultrices",339,1);

  /* Insertion de données dans la table 'film' */

  INSERT INTO `Film` (`titre`,`synopsis`,`duree`,`date_de_sortie`,`realise_par`)
VALUES
  ("eleifend,","enim nec tempus scelerisque, lorem ipsum sodales purus, in molestie tortor nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare sagittis felis. Donec tempor, est ac mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.","3:17","2022-10-06 06:04:39","Dexter Kinney"),
  ("Proin","dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel nisl. Quisque fringilla euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum","3:01","2022-10-18 08:32:11","Britanney Wooten"),
  ("malesuada","ipsum. Phasellus vitae mauris sit amet lorem semper auctor. Mauris vel turpis. Aliquam adipiscing lobortis risus. In mi pede, nonummy ut, molestie in, tempus eu, ligula. Aenean euismod mauris eu elit. Nulla facilisi. Sed neque. Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae Phasellus ornare. Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat,","3:05","2022-10-23 21:15:02","Nerea Holt"),
  ("vitae","mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy ac, feugiat non, lobortis quis, pede. Suspendisse dui. Fusce diam nunc, ullamcorper eu, euismod ac, fermentum vel, mauris. Integer sem elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed et libero. Proin mi. Aliquam gravida mauris ut mi. Duis risus odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi neque tellus, imperdiet non, vestibulum nec, euismod","1:15","2022-10-04 20:53:18","Upton Mcgee"),
  ("egestas","augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel sapien imperdiet ornare. In faucibus. Morbi vehicula. Pellentesque tincidunt tempus risus. Donec egestas. Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing. Mauris molestie","3:44","2022-11-08 04:46:43","Mercedes Ferguson"),
  ("eu","iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec, diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing ligula. Aenean gravida nunc sed pede. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel arcu eu odio tristique pharetra. Quisque ac libero nec ligula consectetuer rhoncus. Nullam velit dui, semper et,","3:01","2022-10-13 17:51:35","Austin Bowers"),
  ("ligula.","sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel arcu eu odio tristique pharetra. Quisque ac libero nec ligula consectetuer rhoncus. Nullam velit dui, semper et, lacinia vitae, sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi metus. Vivamus euismod urna. Nullam lobortis quam a felis ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus","2:59","2022-11-13 02:29:53","Uta Solis"),
  ("consectetuer","in, tempus eu, ligula. Aenean euismod mauris eu elit. Nulla facilisi. Sed neque. Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae Phasellus ornare. Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos","2:32","2022-09-23 21:59:16","Indira Estrada"),
  ("Fusce","vel, venenatis vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit amet ultricies sem magna nec quam.","1:03","2022-11-07 15:17:19","Heather Hester"),
  ("nunc","mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel nisl. Quisque fringilla euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum ultricies","3:41","2022-09-25 04:17:32","Hyatt Kennedy"),
  ("pede","vel arcu eu odio tristique pharetra. Quisque ac libero nec ligula consectetuer rhoncus. Nullam velit dui, semper et, lacinia vitae, sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi metus. Vivamus euismod urna. Nullam lobortis quam a felis ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl.","2:23","2022-10-13 14:11:27","Alana Boyd"),
  ("et","id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt","1:23","2022-11-16 05:56:18","Chancellor French"),
  ("ultricies","enim diam vel arcu. Curabitur ut odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae sodales nisi magna sed dui. Fusce aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in molestie","1:57","2022-10-26 00:30:49","Andrew Lloyd"),
  ("Etiam","faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend, nunc risus varius orci, in consequat enim diam vel arcu. Curabitur ut odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae sodales nisi magna sed","2:38","2022-11-27 06:05:19","Jaquelyn Landry"),
  ("volutpat","vitae erat vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac metus vitae velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique","2:00","2022-10-02 05:11:22","Adria Alexander");

/* Insertion de données dans la table 'Séance' */

INSERT INTO `Seance` (`Date_et_heure_seance`,`id_salle`,`id_Film`)
VALUES
  ("2022-10-14 15:20:15",17,1),
  ("2022-10-11 14:15:34",18,2),
  ("2022-09-21 00:40:36",13,10),
  ("2022-09-20 11:17:28",6,15),
  ("2022-10-20 21:42:33",4,7),
  ("2022-09-21 13:29:58",18,13),
  ("2022-11-06 11:01:25",2,5),
  ("2022-10-05 12:06:20",3,8),
  ("2022-09-22 02:31:38",16,12),
  ("2022-11-19 22:57:17",18,7),
  ("2022-09-22 17:55:04",4,5),
  ("2022-11-04 10:45:26",18,6),
  ("2022-11-12 17:49:19",18,9),
  ("2022-10-31 16:46:03",14,3),
  ("2022-09-25 16:39:49",9,6),
  ("2022-11-23 21:16:32",8,12),
  ("2022-10-11 20:40:47",9,8),
  ("2022-10-04 00:15:17",9,5),
  ("2022-10-13 19:39:33",14,6),
  ("2022-09-20 15:32:09",6,14);

/* Insertion de données dans la table 'utilisateur' */

INSERT INTO `Utilisateur` (`nom`,`prenom`,`e_mail`,`password`,`admin`)
VALUES
  ("Kerr","Claire","claire_kerr9056@protonmail.com","$2y$10$TIgg6/qsvqeK3jkdPg.n.ukSeRIHwMmX3LIdiDguXKN6H/5BJNktG","1"),
  ("Christensen","Fallon","christensen-fallon@yahoo.com","$2y$10$oqnyK8Jau4GgeSD.y7cCCuWW8D7Do2fO8LBhxObaHYQwv/v9ZcLTW","1"),
  ("Burton","Kamal","kamalburton9620@google.com","$2y$10$4dShBXGT5OCs5MJk.dichusObQrqbaa2b1RS4DBcxqcwNcMy8UzbW","1"),
  ("Flynn","Lewis","lewis-flynn7032@icloud.com","$2y$10$yZ7kCAFWPv56leTg3xGjgOCIaxvCkRVNcWe6nEgh5zesc9AQnmBjG","1"),
  ("Gentry","Justine","gentry_justine@hotmail.com","$2y$10$lsTVjfjuccnHVdSFrbGO6O0QEu7RF3cBq5CSHKLCXdZj21zYrEaqq","0"),
  ("William","Keaton","k_william4567@protonmail.com","$2y$10$AvcYnRQrvgDL.VXGBQEXyuPiQ06exZhYq7RaJEiQeL03lRF7snVwu","1"),
  ("Craft","Zephr","czephr@yahoo.com","$2y$10$YdxGgzUp08ytRfyuHNoRruyr8kIPR5V1yMPLvmihUVKJFXW4.EnzK","1"),
  ("Lambert","Ivor","ivor_lambert@outlook.com","$2y$10$Se3ziYq6H4vub6sgtRnZ8u8s.wB3zDWX15r6SbF9WMKv0Dq0OfC.6","1"),
  ("Kelley","Roanna","kelleyroanna@yahoo.com","$2y$10$5RCWrPcDVfyayZ.Mle3PD.iIOhNopZbs4BX7/aP5z6H5bNm0ECMQa","0"),
  ("Dorsey","Zeus","d.zeus@icloud.com","$2y$10$lSZY3CFO3lzRKfvhsRcPbOJ.Pmo0JEx/BKtIwIl8gXPa9riN1IAei","0");

/* Insertion de données dans la table 'Réservation' */

INSERT INTO `Reservation` (`created`,`id_seance`,`id_utilisateur`)
VALUES
  ("2023-05-01 23:48:14",5,7),
  ("2022-09-16 02:41:25",19,3),
  ("2023-01-11 14:25:31",2,7),
  ("2023-05-26 11:00:48",10,6),
  ("2023-04-02 04:30:19",15,8),
  ("2023-06-28 21:09:30",10,7),
  ("2023-06-23 00:24:20",15,4),
  ("2023-04-27 02:00:37",12,3),
  ("2023-03-19 09:01:54",7,2),
  ("2023-06-03 21:07:14",16,10),
  ("2022-10-12 05:10:23",17,3),
  ("2022-11-02 07:05:08",11,7),
  ("2023-05-24 12:11:27",17,7),
  ("2022-12-17 07:57:40",12,4),
  ("2022-11-17 18:59:29",17,4),
  ("2023-08-24 06:54:58",6,8),
  ("2022-10-10 15:40:20",2,8),
  ("2022-09-08 03:18:02",8,8),
  ("2023-02-14 15:43:19",11,3),
  ("2023-08-16 13:50:57",5,5);

  /* Insertion de données dans la table 'Paiement' */

INSERT INTO `paiement` (`type_de_paiement`,`created`,`id_reservation`)
VALUES
  ("en ligne","2023-05-01 23:48:14",1),
  ("sur place","2022-09-16 02:41:25",2),
  ("sur place","2023-01-11 14:25:31",3),
  ("en ligne","2023-05-26 11:00:48",4),
  ("sur place.","2023-04-02 04:30:19",5),
  ("en ligne","2023-06-28 21:09:30",6),
  ("sur place","2023-06-23 00:24:20",7),
  ("en ligne","2023-04-27 02:00:37",8),
  ("sur place","2023-03-19 09:01:54",9),
  ("sur place","2023-06-03 21:07:14",10),
  ("en ligne","2022-10-12 05:10:23",11),
  ("en ligne","2022-11-02 07:05:08",12),
  ("en ligne","2023-05-24 12:11:27",13),
  ("sur place","2022-12-17 07:57:40",14),
  ("en ligne,","2022-11-17 18:59:29",15),
  ("en ligne","2023-08-24 06:54:58",16),
  ("sur place","2022-10-10 15:40:20",17),
  ("sur place","2022-09-08 03:18:02",18),
  ("sur place","2023-02-14 15:43:19",19),
  ("sur place","2023-08-16 13:50:57",20);

  /* Insertion de données dans la table 'Tarif' */

INSERT INTO `Tarif` (`nom`, `prix`,`description`)
VALUES
  ("Plein tarif","9.20","tempus scelerisque, lorem ipsum sodales purus, in molestie tortor nibh"),
  ("Etudiant","7.60","tempus scelerisque, lorem ipsum sodales purus, in molestie tortor nibh"),
  ("Moins de 14 ans","5.90","tempus scelerisque, lorem ipsum sodales purus, in molestie tortor nibh");

  /* Insertion de données dans la table 'Tarifs_seances' */

INSERT INTO `Tarifs_Seances` (`id_seance`,`id_tarif`)
VALUES
  ("1","1"),
  ("1","2"),
  ("1","3"),
  ("2","1"),
  ("2","2"),
  ("2","3"),
  ("3","1"),
  ("3","2"),
  ("3","3"),
  ("4","1"),
  ("4","2"),
  ("4","3"),
  ("5","1"),
  ("5","2"),
  ("5","3"),
  ("6","1"),
  ("6","2"),
  ("6","3"),
  ("7","1"),
  ("7","2"),
  ("7","3"),
  ("8","1"),
  ("8","2"),
  ("8","3"),
  ("9","1"),
  ("9","2"),
  ("9","3"),
  ("10","1"),
  ("10","2"),
  ("10","3"),
  ("11","1"),
  ("11","2"),
  ("11","3"),
  ("12","1"),
  ("12","2"),
  ("12","3"),
  ("13","1"),
  ("13","2"),
  ("13","3"),
  ("14","1"),
  ("14","2"),
  ("14","3"),
  ("15","1"),
  ("15","2"),
  ("15","3"),
  ("16","1"),
  ("16","2"),
  ("16","3"),
  ("17","1"),
  ("17","2"),
  ("17","3"),
  ("18","1"),
  ("18","2"),
  ("18","3"),
  ("19","1"),
  ("19","2"),
  ("19","3"),
  ("20","1"),
  ("20","2"),
  ("20","3");